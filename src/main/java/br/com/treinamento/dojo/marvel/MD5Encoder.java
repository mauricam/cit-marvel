package br.com.treinamento.dojo.marvel;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Encoder {

	private String ts;
	private String publicKey;
	private String privateKey = "7249280f694a51ccbcb01c3bfc9287b1774dabfe";

	public MD5Encoder(String ts, String publicKey) {
		this.ts = ts;
		this.publicKey = publicKey;
	}
	
	
	public String encode() {
	
		String authString = ts + privateKey + publicKey;
		
		MessageDigest md5 = createMD5Digest();
		
		try {
			byte[] hash = md5.digest(authString.getBytes("UTF-8"));
			return asHexString(hash);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}


	private MessageDigest createMD5Digest() {
		MessageDigest md5 = null;
		try {
			md5 = java.security.MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		return md5;
	}
	
	private String asHexString(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
		    if ((0xff & hash[i]) < 0x10) {
		        hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
		    } else {
		        hexString.append(Integer.toHexString(0xFF & hash[i]));
		    }
		}
		return hexString.toString();
	}
	
}