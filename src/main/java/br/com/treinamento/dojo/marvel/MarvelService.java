package br.com.treinamento.dojo.marvel;

import java.util.Date;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

public class MarvelService {

	
	private String publicKey = "ac021d7dfbb72cb7416c1eb89e2ccec5";
	
	
	private Client client = ClientBuilder.newClient();
	private WebTarget base = client.target("http://gateway.marvel.com/v1/public");
	private WebTarget series = base.path("series");
	private WebTarget characters = base.path("characters");
	private WebTarget creators = base.path("creators");

	
	
	

	public Map<String, Object> getSeries() {
		return getResource(series);
	}
	
	public Map<String, Object> getCharacters() {
		return getResource(characters);
	}

	public Map<String, Object> getCreators() {
		return getResource(creators);
	}

	
	public Map<String, Object> getResource(WebTarget path)  {
		
		Date d = new Date();
		String ts = Long.toString(d.getTime());
		
		
		String hash = new MD5Encoder(ts, publicKey).encode();
		
		
		WebTarget target = path.
				queryParam("ts", ts).
				queryParam("apikey", publicKey).
				queryParam("hash", hash);

		Builder builder = target.request();
		Response response = builder.get();
		
		String s = response.readEntity(String.class);
		
		@SuppressWarnings("unchecked")
		Map<String, Object> map  = new Gson().fromJson(s, Map.class);
		
		return map;
	}


	


	
}
