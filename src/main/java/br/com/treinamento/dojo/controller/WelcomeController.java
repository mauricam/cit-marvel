package br.com.treinamento.dojo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.marvel.MarvelService;

@RestController
public class WelcomeController {

	@RequestMapping(value = "/helloworld", method = RequestMethod.GET)
	public ResponseEntity<String> helloWorld() {
		return new ResponseEntity<String>("Hello World, this is my first SpringMVC application!", HttpStatus.OK);
	}

	

	@RequestMapping(value = "/comics", method = RequestMethod.GET)
	public ResponseEntity<String> getMarvelComics(
			@RequestParam(value="index", required=false) Integer index, 
			HttpServletRequest request) {
		
		if (index == null) {
			return loadComicsInSession(request);

		} else {
			return getElementByIndex(index, request);
		}		
	}

	

	private ResponseEntity<String> loadComicsInSession(HttpServletRequest request) {
		
		MarvelService service = new MarvelService();
		Map<String, Object> series = service.getSeries();
		
		request.getSession().setAttribute("series", series);
		
		return new ResponseEntity<String>("Loaded list of 20 comics series", HttpStatus.OK);
	}


	@SuppressWarnings("unchecked")
	private ResponseEntity<String> getElementByIndex(Integer index, HttpServletRequest request) {
		
		
		Map<String, Object> series = (Map<String, Object>) request.getSession().getAttribute("series");
		if (series.isEmpty()) {
			String response = "no comics available";
			return new ResponseEntity<String>(response, HttpStatus.NO_CONTENT);
		}
		
		Map<String, Object> data = (Map<String, Object>) series.get("data");
		List<Object> results = (List<Object>) data.get("results");
		Map<String, Object> comic = (Map<String, Object>) results.get(index);
		
		String response = "Data for comic number " + index + " in the series: " + comic.toString();
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}



	
	
}
