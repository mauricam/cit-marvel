package br.com.treinamento.dojo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;


public class WelcomeControllerTest {

	private HttpServletRequest request;
	private HttpSession session;
	
	@Before
	public void begin() {

		request = Mockito.mock(HttpServletRequest.class);
		session = Mockito.mock(HttpSession.class);
		Mockito.when(request.getSession()).thenReturn(session);

	}
	
	@Test
	public void testLoadComics() {
		
		ResponseEntity<String> response = new WelcomeController().getMarvelComics(null, request);
		Assert.assertEquals(200, response.getStatusCode().value());
		Assert.assertEquals("Loaded list of 20 comics series", response.getBody());
		
	}
	
	
	@Test
	public void testComicsNotLoaded() {
		
		Mockito.when(session.getAttribute("series")).thenReturn(new HashMap<>());
		
		ResponseEntity<String> response = new WelcomeController().getMarvelComics(1, request);
		
		Assert.assertEquals(204, response.getStatusCode().value());
		Assert.assertEquals("no comics available", response.getBody());
		
	}
	
	@Test
	public void testGetComicsByIndex() {
		
		Map<String, Object> map = new HashMap<>();
		map.put("code", 200);
		Map<String, Object> data = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> aComic = new HashMap<>();
		aComic.put("index", 0);
		list.add(aComic);
		aComic = new HashMap<>();
		aComic.put("index", 1);
		list.add(aComic);
		data.put("results", list );
		map.put("data", data);
		
		Mockito.when(session.getAttribute("series")).thenReturn(map);
		
		int index = 1;
		
		ResponseEntity<String> response = new WelcomeController().getMarvelComics(index, request);
		Assert.assertEquals(200, response.getStatusCode().value());
		Assert.assertEquals("Data for comic number 1 in the series: {index="+index+"}", response.getBody());
		
	}
	
}
