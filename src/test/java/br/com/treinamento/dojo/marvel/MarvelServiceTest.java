package br.com.treinamento.dojo.marvel;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;



public class MarvelServiceTest {

	@Test
	public void listSeries() {
		MarvelService service = new MarvelService();
		Map<String, Object> response = service.getSeries();
		Assert.assertEquals(200.0, response.get("code"));		

		System.out.println(response);
		
		Map data = (Map) response.get("data");
		List results = (List) data.get("results");
		Assert.assertEquals(20, results.size());
		System.out.println(results);
	}

	@Test
	public void listCharacters() {
		MarvelService service = new MarvelService();
		Map<String, Object> response = service.getCharacters();
		System.out.println(response);
		Assert.assertEquals(200.0, response.get("code"));		
	}

	@Test
	public void listCreators() {
		MarvelService service = new MarvelService();
		Map<String, Object> response = service.getCreators();
		System.out.println(response);
		Assert.assertEquals(200.0, response.get("code"));
		
		
	}

	
	
}
